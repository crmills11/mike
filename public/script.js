Survey.Survey.cssType = "bootstrap";

var json = {
    title: "Knowledge Test",
    showProgressBar: "bottom",
    showTimerPanel: "top",
    maxTimeToFinishPage: 10,
    maxTimeToFinish: 100,
    firstPageIsStarted: true,
    startSurveyText: "Start Quiz",
    pages: [
        {
            questions: [
                {
                    type: "html",
                    html: "You are about to start a true/flase quiz. <br/>You have 10 seconds for every question and 120 seconds for the whole survey of 10 questions.<br/>Please click on <b>'Start Quiz'</b> button when you are ready."
                }
            ]
        }, {
            questions: [
                {
                    type: "radiogroup",
                    name: "The first step of Active Listening is Receiving.",
                    title: "The first step of Active Listening is Receiving.",
                    choices: [
                        "True","False"
                    ],
                    correctAnswer: "False"
                }
            ]
        }, {
            questions: [
                {
                    type: "radiogroup",
                    name: "Respond is when you allow the speaker to finish talking, then you respond. ",
                    title: "Respond is when you allow the speaker to finish talking, then you respond. ",
                    choices: [
                        "True","False"
                    ],
                    correctAnswer: "True"
                }
            ]
        }, {
            questions: [
                {
                    type: "radiogroup",
                    name: "There are four steps involved with Active Listening.",
                    title: "There are four steps involved with Active Listening.",
                    choices: [
                        "True","False"
                    ],
                    correctAnswer: "False"
                }
            ]
        }, {
            questions: [
                {
                    type: "radiogroup",
                    name: "Review is the last step in Active Listening.",
                    title: "Review is the last step in Active Listening.",
                    choices: [
                        "True","False"
                    ],
                    correctAnswer: "False"
                }
            ]
        }, {
            questions: [
                {
                    type: "radiogroup",
                    name: "Review is the step where you commit what you’ve just heard to memory.",
                    title: "Review is the step where you commit what you’ve just heard to memory.",
                    choices: [
                        "True","False"
                    ],
                    correctAnswer: "False"
                }
            ]
        }, {
            questions: [
                {
                    type: "radiogroup",
                    name: "Receive is when you hear the message.",
                    title: "Receive is when you hear the message.",
                    choices: [
                        "True","False"
                    ],
                    correctAnswer: "True"
                }
            ]
        }, {
            questions: [
                {
                    type: "radiogroup",
                    name: "Review and Remember are interchangeable.",
                    title: "Review and Remember are interchangeable.",
                    choices: [
                        "True","False"
                    ],
                    correctAnswer: "False"
                }
            ]
        }, {
            questions: [
                {
                    type: "radiogroup",
                    name: "Getting caught up on a word a speaker has said while they’re talking is an example of effective listening.",
                    title: "Getting caught up on a word a speaker has said while they’re talking is an example of effective listening.",
                    choices: [
                        "True","False"
                    ],
                    correctAnswer: "False"
                }
            ]
        }, {
            questions: [
                {
                    type: "radiogroup",
                    name: "Ready, Receive, Review, Respond, Remember are 5 steps that make up Active Listening.",
                    title: "Ready, Receive, Review, Respond, Remember are 5 steps that make up Active Listening.",
                    choices: [
                        "True","False"
                    ],
                    correctAnswer: "True"
                }
            ]
        }, 
    ],
    completedHtml: "<h4>You have answered correctly <b>{correctedAnswers}</b> questions from <b>{questionCount}</b>.</h4>"
};

var survey = new Survey.Model(json);

survey
  .onComplete
  .add(function(result) {
    document
      .querySelector('#surveyResult')
      .innerHTML = "result: " + JSON.stringify(result.data);
  });

$("#surveyElement").Survey({
  model: survey
});
